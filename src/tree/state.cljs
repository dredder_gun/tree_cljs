(ns tree.state
  (:require [reagent.core :as reagent :refer [atom]]
            [clojure.walk :refer [keywordize-keys]]
            [tree.helpers :refer [disable-focus]]
            [webpack.bundle]))

(defonce stateRT (atom {:treeData [{:title "Карточка 1" :active true}
                                   {:title "Картчока 2" :active false :expanded true :children [{ :title "Карточка 2.1" :active false}]}]
                        :activeEl nil}))

(defn make-map-to-js-function
  [cljs_element new_key_val_vec]
  (let [set-new-node (partial assoc (:node cljs_element))]
    {:treeData (disable-focus (:treeData @stateRT))
     :holeElement cljs_element
     :path (:path cljs_element)
     :getNodeKey (fn [params] (.-treeIndex params)) ; suppose that params is js object
     :newNode (apply set-new-node new_key_val_vec)}))

(defn execute-and-new-state
  [updated_data]
  (let [js-func (.. js/window -deps -changeNodeAtPath)]
    [:activeEl (:holeElement updated_data)
     :treeData (keywordize-keys (js->clj (js-func (clj->js updated_data))))]))

(defn change-atom-state
  [keys-to-update]
  (let [swap-state (partial swap! stateRT assoc)]
    (apply swap-state keys-to-update)))

(defn debug
  [param]
  (prn "debug fun" param)
  param)

(defn change-title
  [cljs_element new-title]
  (->> (make-map-to-js-function cljs_element [:title new-title])
        execute-and-new-state
        change-atom-state))

(defn changeNodeFields
  [cljs_element new-title]
  (->> (make-map-to-js-function cljs_element [:title new-title])
        execute-and-new-state
        change-atom-state))

(defn deactivate-node
  [cljs_element]
  (->> (make-map-to-js-function cljs_element [:active false])
        execute-and-new-state
        change-atom-state))

(defn activate-node
  [cljs_element]
  (if-let [current-el (:activeEl @stateRT)]
    (->> (make-map-to-js-function cljs_element [:active true])
        execute-and-new-state
        change-atom-state)
    (->> (make-map-to-js-function cljs_element [:active true])
          execute-and-new-state
          change-atom-state)))

(defn save-form
  [cljs_element form-values]
  (let [title (:title form-values)
        description (:description form-values)
        rank (:rank form-values)
        priority (:priority form-values)
        responsible (:responsible form-values)
        tags (:tags form-values)]
    (->> (make-map-to-js-function cljs_element [:title title
                                                 :description description
                                                 :rank rank
                                                 :priority priority
                                                 :responsible responsible
                                                 :tags tags])
        execute-and-new-state
        change-atom-state)))
