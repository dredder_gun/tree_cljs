(ns tree.card
  (:require [reagent.core :as reagent :refer [atom]]
            [tree.state :refer [change-title save-form]]
            [webpack.bundle]))

(defn card
  [activeEl activePath]
  (let [node_ae (:node activeEl)
        form-values (atom {:title (:title node_ae)
                          :description (:description node_ae)
                          :rank (:rank node_ae)
                          :priority (:priority node_ae)
                          :responsible (:responsible node_ae)
                          :tags (:tags node_ae)})
        current-card (atom nil)]
    (fn [activeEl activePath]
      (when (not= activeEl @current-card)
        (reset! current-card activeEl)
        (swap! form-values assoc :title (:title (:node activeEl))
                                  :description (:description (:node activeEl))
                                  :rank (:rank (:node activeEl))
                                  :priority (:priority (:node activeEl))
                                  :responsible (:responsible (:node activeEl))
                                  :tags (:tags (:node activeEl))))
      [:div.basic-grey
        [:h1 "Параметры карточки"]
        [:form.basic-grey
          {:on-submit #(do
                          (.preventDefault %)
                          (save-form activeEl @form-values))}
          ;
          [:label [:span "Заголовок:"] [:textarea {:name "name"
                                                   :onChange #(swap! form-values assoc :title (.. % -target -value))
                                                   :value (:title @form-values)
                                                   :on-blur #(change-title activeEl (:title @form-values))}]]
          ;
          [:label [:span "Описание:"] [:textarea {:name "description"
                                                  :onChange #(swap! form-values assoc :description (.. % -target -value))
                                                  :value (:description @form-values)}]]
          ;
          [:label [:span "Баллы:"] [:input {:name "rank"
                                            :onChange #(swap! form-values assoc :rank (.. % -target -value))
                                            :value (:rank @form-values)}]]
          ;
          [:label [:span "Приоритет:"] [:input {:name "priority"
                                                :onChange #(swap! form-values assoc :priority (.. % -target -value))
                                                :value (:priority @form-values)}]]
          ;
          [:label [:span "Ответственный:"] [:input {:name "responsible"
                                                    :onChange #(swap! form-values assoc :responsible (.. % -target -value))
                                                    :value (:responsible @form-values)}]]
          ;
          [:label [:span "Теги:"] [:input {:name "tags"
                                           :onChange #(swap! form-values assoc :tags (.. % -target -value))
                                           :value (:tags @form-values)}]]
          ;
          [:label [:span (symbol "&nbsp;")] [:input.button {:type "submit" :value "Сохранить"}]]]])))
