(ns tree.card-input
  (:require [reagent.core :as reagent :refer [atom]]
            [tree.helpers :refer [initial-focus-wrapper]]
            [clojure.walk :refer [keywordize-keys]]
            [tree.state :refer [stateRT change-title activate-node deactivate-node]]
            [webpack.bundle]))

(defn inner-input
  [cljs_element activeEl title]
    (reagent/with-let [form (atom title)]
      [:input {:type "text"
             :onChange #(reset! form (.. % -target -value))
             :on-blur #(change-title cljs_element (.. % -target -value))
             :style {:display (if (:active (:node cljs_element)) "block" "none")
                     :width (* (+ 1 (count @form)) 8)
                     :max-width 730
                     :min-width 170}
             :value @form}]))

(defn card-input
  [cljs_element activeEl]
    (fn [cljs_element activeEl]
      (let [title (:title (:node cljs_element))]
        [:div
          [:div {:on-double-click #(activate-node cljs_element)
                 :style {:display (if (:active (:node cljs_element)) "none" "block")
                         :min-width 170
                         :height 20}}
            title]
            (if (:active (:node cljs_element))
              [initial-focus-wrapper
                (reagent/as-element
                    [inner-input cljs_element activeEl title])]
              ;
              [inner-input cljs_element activeEl title])])))
