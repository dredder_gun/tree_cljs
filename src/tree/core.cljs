(ns tree.core
  (:require [reagent.core :as reagent :refer [atom]]
            [tree.card :refer [card]]
            [tree.card-input :refer [card-input]]
            [tree.state :refer [activate-node stateRT]]
            [tree.helpers :refer [disable-focus]]
            [clojure.walk :refer [keywordize-keys]]
            [webpack.bundle]))

;; -------------------------
;; Views

(defn home-page []

  (let [react-tree (.. js/window -deps -reacSortableTree)
        addNodeUnderParent (.. js/window -deps -addNodeUnderParent)
        removeNodeAtPath (.. js/window -deps -removeNodeAtPath)]
    [:div
     [:h2 "Редактор деревьев"]
     [:div#main-wrapper
       [:div#left-wrapper
         [:button {:on-click #(swap! stateRT update :treeData conj {:title "Новый узел" :active true})} "Добавить"]
         [:button "JSON!"]
         [:div#react-tree-wrapper
           [:> react-tree {:treeData (:treeData @stateRT)
                           :className "sortable-tree"
                           :onChange #(swap! stateRT assoc :treeData (keywordize-keys (js->clj %)))
                           :generateNodeProps (fn [element]
                                               (let [cljs_element (keywordize-keys (js->clj element))]
                                                   #js
                                                   { :buttons
                                                       #js
                                                         [(reagent/create-element
                                                             "button"
                                                             #js{:onClick #(let [tree_with_new_node (addNodeUnderParent
                                                                                                      (clj->js
                                                                                                         {:treeData (disable-focus (:treeData @stateRT))
                                                                                                         :parentKey (let [path (:path cljs_element)]
                                                                                                                        (nth path (- (count path) 1)))
                                                                                                         :expandParent true
                                                                                                         :getNodeKey (fn [params] (.-treeIndex params))
                                                                                                         :newNode
                                                                                                           {:title "Новая карточка",
                                                                                                            :description "",
                                                                                                            :rank "",
                                                                                                            :priority "",
                                                                                                            :responsible "",
                                                                                                            :tags "",
                                                                                                            :active true}}))]
                                                                            (swap! stateRT assoc :treeData (keywordize-keys
                                                                                                             (js->clj (.-treeData tree_with_new_node)))))}
                                                                  ;
                                                                  "+")
                                                          (reagent/create-element
                                                            "button"
                                                            #js{:onClick #(swap! stateRT assoc :treeData (keywordize-keys
                                                                                                            (js->clj
                                                                                                              (removeNodeAtPath (clj->js
                                                                                                                {:treeData (:treeData @stateRT)
                                                                                                                :path (:path cljs_element)
                                                                                                                :getNodeKey (fn [params] (.-treeIndex params))})))))}
                                                          "-")]
                                                       :title (reagent/as-element
                                                                 [card-input cljs_element (:activeEl @stateRT)])}))}]]]
          [:div#right-wrapper
            [card (:activeEl @stateRT)]]]]))

;; -------------------------
;; Initialize app

(defn mount-root []
  (reagent/render [home-page] (.getElementById js/document "app")))

(defn init! []
  (mount-root))
