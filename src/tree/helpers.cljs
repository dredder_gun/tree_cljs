(ns tree.helpers
  (:require [reagent.core :as reagent :refer [atom]]))

(def initial-focus-wrapper
  (with-meta identity
    {:component-did-mount #(.focus (reagent/dom-node %))
     :component-will-update #(prn "initial-focus-wrapper")}))

(defn disable-focus
  [treeData]
    (mapv #(if (:children %)
            (assoc % :active false :children (disable-focus (:children %)))
            (assoc % :active false))
    treeData))
