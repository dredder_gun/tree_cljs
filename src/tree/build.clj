(ns tree.build
  (:gen-class)
  (:require [cljs.build.api :as b]))

; Пока не используется

(defn -main
  [& args]
  ; (try
  (b/build "src"
    {:output-dir "js/out"
     :output-to "js/out/main.js"
     :optimizations :none
     :main 'tree.core
     :install-deps true
     :npm-deps {:react "^16.0.0"
                :react-dom "^16.0.0"
                :react-sortable-tree "^1.5.0"}})
  ; (catch Exception e
  ;   (prn "Error while build clojurescript: " (.getMessage e)))
  (prn "Main method"))
