import SortableTree, { addNodeUnderParent, removeNodeAtPath, changeNodeAtPath } from 'react-sortable-tree';

window.deps = {
    'react' : require('react'),
    'react-dom' : require('react-dom'),
    'reacSortableTree' : SortableTree,
    'addNodeUnderParent' : addNodeUnderParent,
    'changeNodeAtPath' : changeNodeAtPath,
    'removeNodeAtPath' : removeNodeAtPath
};

window.React = window.deps['react'];
window.ReactDOM = window.deps['react-dom'];
