// Compiled by ClojureScript 1.9.946 {:static-fns true, :optimize-constants true}
goog.provide('reagent.debug');
goog.require('cljs.core');
goog.require('cljs.core.constants');
reagent.debug.has_console = typeof console !== 'undefined';
reagent.debug.tracking = false;
if(typeof reagent.debug.warnings !== 'undefined'){
} else {
reagent.debug.warnings = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
if(typeof reagent.debug.track_console !== 'undefined'){
} else {
reagent.debug.track_console = (function (){var o = ({});
o.warn = ((function (o){
return (function() { 
var G__13619__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.cst$kw$warn], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__13619 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__13620__i = 0, G__13620__a = new Array(arguments.length -  0);
while (G__13620__i < G__13620__a.length) {G__13620__a[G__13620__i] = arguments[G__13620__i + 0]; ++G__13620__i;}
  args = new cljs.core.IndexedSeq(G__13620__a,0,null);
} 
return G__13619__delegate.call(this,args);};
G__13619.cljs$lang$maxFixedArity = 0;
G__13619.cljs$lang$applyTo = (function (arglist__13621){
var args = cljs.core.seq(arglist__13621);
return G__13619__delegate(args);
});
G__13619.cljs$core$IFn$_invoke$arity$variadic = G__13619__delegate;
return G__13619;
})()
;})(o))
;

o.error = ((function (o){
return (function() { 
var G__13622__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.cst$kw$error], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__13622 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__13623__i = 0, G__13623__a = new Array(arguments.length -  0);
while (G__13623__i < G__13623__a.length) {G__13623__a[G__13623__i] = arguments[G__13623__i + 0]; ++G__13623__i;}
  args = new cljs.core.IndexedSeq(G__13623__a,0,null);
} 
return G__13622__delegate.call(this,args);};
G__13622.cljs$lang$maxFixedArity = 0;
G__13622.cljs$lang$applyTo = (function (arglist__13624){
var args = cljs.core.seq(arglist__13624);
return G__13622__delegate(args);
});
G__13622.cljs$core$IFn$_invoke$arity$variadic = G__13622__delegate;
return G__13622;
})()
;})(o))
;

return o;
})();
}
reagent.debug.track_warnings = (function reagent$debug$track_warnings(f){
reagent.debug.tracking = true;

cljs.core.reset_BANG_(reagent.debug.warnings,null);

(f.cljs$core$IFn$_invoke$arity$0 ? f.cljs$core$IFn$_invoke$arity$0() : f.call(null));

var warns = cljs.core.deref(reagent.debug.warnings);
cljs.core.reset_BANG_(reagent.debug.warnings,null);

reagent.debug.tracking = false;

return warns;
});
