// Compiled by ClojureScript 1.9.946 {:static-fns true, :optimize-constants true}
goog.provide('tree.prod');
goog.require('cljs.core');
goog.require('cljs.core.constants');
goog.require('tree.core');
cljs.core._STAR_print_fn_STAR_ = (function() { 
var G__14118__delegate = function (_){
return null;
};
var G__14118 = function (var_args){
var _ = null;
if (arguments.length > 0) {
var G__14119__i = 0, G__14119__a = new Array(arguments.length -  0);
while (G__14119__i < G__14119__a.length) {G__14119__a[G__14119__i] = arguments[G__14119__i + 0]; ++G__14119__i;}
  _ = new cljs.core.IndexedSeq(G__14119__a,0,null);
} 
return G__14118__delegate.call(this,_);};
G__14118.cljs$lang$maxFixedArity = 0;
G__14118.cljs$lang$applyTo = (function (arglist__14120){
var _ = cljs.core.seq(arglist__14120);
return G__14118__delegate(_);
});
G__14118.cljs$core$IFn$_invoke$arity$variadic = G__14118__delegate;
return G__14118;
})()
;
tree.core.init_BANG_();
