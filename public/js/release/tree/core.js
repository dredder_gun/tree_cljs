// Compiled by ClojureScript 1.9.946 {:static-fns true, :optimize-constants true}
goog.provide('tree.core');
goog.require('cljs.core');
goog.require('cljs.core.constants');
goog.require('reagent.core');
tree.core.home_page = (function tree$core$home_page(){
var react_tree = (window["deps"]["react-sortable-tree"]);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.cst$kw$div,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.cst$kw$h2,"React sortable tree"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.cst$kw$_GT_,react_tree,new cljs.core.PersistentArrayMap(null, 2, [cljs.core.cst$kw$treeData,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [cljs.core.cst$kw$title,"main",cljs.core.cst$kw$subtitle,"sub"], null),new cljs.core.PersistentArrayMap(null, 3, [cljs.core.cst$kw$title,"value2",cljs.core.cst$kw$expanded,true,cljs.core.cst$kw$children,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 1, [cljs.core.cst$kw$title,"value3"], null)], null)], null)], null),cljs.core.cst$kw$onChange,((function (react_tree){
return (function (){
return cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["onChange works"], 0));
});})(react_tree))
], null)], null)], null);
});
tree.core.mount_root = (function tree$core$mount_root(){
return reagent.core.render.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [tree.core.home_page], null),document.getElementById("app"));
});
tree.core.init_BANG_ = (function tree$core$init_BANG_(){
return tree.core.mount_root();
});
